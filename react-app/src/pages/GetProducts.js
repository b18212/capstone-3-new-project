import {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard';
import {Row} from 'react-bootstrap'
import './ProductGet.css';


export default function GetProducts({product}){

		const [products, setProducts] = useState([])

		const styleObj ={
			fontSize: 50,
			color: "#ff9900"
		}

			useEffect(() => {
				fetch('http://localhost:4000/products/viewAll')
				.then(res => res.json())
				.then(data => {
					console.log(data)



			const allProducts = (data.map(product => {

						return(
							<ProductCard key ={product.id} productProps={product} breakpoint={3}/>
							)
					}))
					setProducts(allProducts)
				})
			},[product])

	return(
	<div className="bg-catalog">
	<div className="bg-catalog-inner">
	<p className="name_top">Check our PRODUCTS</p>
	<Row className="m-5">
				{products}
		</Row>
		</div>
	</div>

		)
		

}