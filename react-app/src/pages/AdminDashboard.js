import { Form, Button, Container, Row, Col, InputGroup } from 'react-bootstrap';

import { useState } from 'react';

export default function AdminDashboard() {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(0);
  const [categories, setCategories] = useState([]);

  fetch('http://localhost:4000/products/getCategories')
    .then((result) => result.json())
    .then((data) => setCategories(data));

  const category = categories.map((data) => {
    return <option value={data}>{data}</option>;
  });

  return (
    <Container>
      <h1 className='mt-5'>Admin Dashboard</h1>
      <Form className='mt-3 mb-5'>
        <Row xs={12} md={12}>
          <Form.Group as={Col} className='mb-3' controlId='productName'>
            <Form.Label>Product Name</Form.Label>
            <Form.Control
              type='text'
              placeholder='Enter company name'
              value={name}
              onChange={(e) => setName(e)}
            />
          </Form.Group>

          <Form.Group as={Col} className='mb-3' controlId='price'>
            <Form.Label>Price</Form.Label>
            <InputGroup className='mb-2'>
              <InputGroup.Text>£</InputGroup.Text>
              <Form.Control
                type='number'
                rows={3}
                value={price}
                onChange={(e) => setPrice(e)}
              />
            </InputGroup>
          </Form.Group>
        </Row>

        <Form.Group className='mb-3' controlId='description'>
          <Form.Label>Product description</Form.Label>
          <Form.Control
            as='textarea'
            rows={3}
            value={description}
            onChange={(e) => setDescription(e)}
          />
        </Form.Group>

        <Row className='mb-3'>
          <Form.Group as={Col} className='mb-3' controlId='quantity'>
            <Form.Label>Quantity</Form.Label>
            <Form.Control
              type='number'
              rows={3}
              value={quantity}
              onChange={(e) => setQuantity(e)}
            />
          </Form.Group>

          <Form.Group as={Col} controlId='formGridState'>
            <Form.Label>Category</Form.Label>
            <Form.Select defaultValue='Choose...'>
              <option>Choose...</option>
              {category}
            </Form.Select>
          </Form.Group>
        </Row>

        <Button variant='primary' type='submit'>
          Submit
        </Button>
      </Form>
    </Container>
  );
}
