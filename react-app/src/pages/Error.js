import {BsFillArrowRightCircleFill} from 'react-icons/bs'
import {Link} from 'react-router-dom';

export default function Error() {
	return(
		<div className="error_page"
					style={{
						margin: '10%'
								}}>
		<img src="imgs/Robot_idle_new.webp"
        			alt="logo" 
        			style={{
        				width:'10%'
      							  }}/>
      							  
		<h1>404 Not-Found</h1>
			<h5>Go back ...</h5>
				<Link as={Link} to="/">
					<BsFillArrowRightCircleFill size={50}/>
				</Link>
			</div>
		)
}