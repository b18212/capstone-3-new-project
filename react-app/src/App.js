import "./App.css";
import RegisterUser from "./pages/RegisterUser";
import RegisterSeller from "./pages/RegisterSeller";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import GetProducts from "./pages/GetProducts";
import SingleProductView from "./pages/SingleProductView";
import { UserProvider } from "./UserContext";
import { useState, useEffect } from "react";
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Footer from "./components/Footer";
import AdminDashboard from "./pages/AdminDashboard";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  // Main products={products}></Main>

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch("http://localhost:4000/customer/getSingleUser", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // captured the data of whoever is logged in
        console.log(data);

        // set the user states values with the user details upon successful login
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        } else {
          // set back the initial state of user
          setUser({
            id: null,
            isAdmin: null,
          });
        }
      });
  }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />

        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/admin" element={<AdminDashboard />} />
          <Route exact path="/products" element={<GetProducts />} />
          <Route
            exact
            path="/products/:productId"
            element={<SingleProductView />}
          />
          <Route exact path="/register" element={<RegisterUser />} />
          <Route exact path="/register_seller" element={<RegisterSeller />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/logout" element={<Logout />} />
          <Route exact path="*" element={<Error />} />
        </Routes>

        <Footer />
      </Router>
    </UserProvider>
  );
}

export default App;
