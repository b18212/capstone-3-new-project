import React from "react";
import { Row, Col, Card, Button } from "react-bootstrap";
import "./Banner.css";
import "./Featured.css";

export default function Featured() {
  return (
    <div className="featured">
      <Row className="m-5">
        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/398409236.png"
                alt="{Image}"
              />
            </Card.Body>

            <Button class="btn">View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Shoes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/197102040.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/322396794.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/41yHQ2pUhAL.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>
      </Row>

      <Row className="m-5">
        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/398409236.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Shoes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/197102040.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/322396794.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>

        <Col xs={12} md={6} lg={3}>
          <Card className="featured p-3 mb-5">
            <Card.Body>
              <Card.Title>
                <h2>Dad's Clothes</h2>
              </Card.Title>

              <Card.Text>Best Products for Dad's Clothes.</Card.Text>
              <Card.Img
                src="https://images-na.ssl-images-amazon.com/images/G/01/AmazonExports/Events/2021/FathersDay/41yHQ2pUhAL.png"
                alt="{Image}"
              />
            </Card.Body>
            <Button>View All Products</Button>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
