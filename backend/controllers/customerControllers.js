const bcrypt = require('bcryptjs');
const Customer = require('../models/Customer');
const Product = require('../models/Product');
const Seller = require('../models/Seller');
const auth = require('../security/auth');

// NOTE: Register user
let registerCustomer = (req, res) => {
  const { firstName, lastName, email, password, mobileNumber } = req.body;
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync(password, salt);

  let newCustomer = new Customer({
    firstName,
    lastName,
    email,
    password: hashPassword,
    mobileNumber,
  });

  newCustomer
    .save()
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

// NOTE: Check if email exists
let checkIfEmailExist = (req, res) => {
  const { email } = req.body;

  Customer.countDocuments({ email }, (err, count) => {
    if (err) {
      console.log(err);
    }

    if (count === 0) {
      Seller.countDocuments({ email }, (err, document) => {
        if (document === 0) {
          res.send(false);
        } else {
          Seller.findOne({ email })
            //Return true IF the seller Email exists
            .then((result) => res.send(true))
            .catch((err) => res.send(err));
        }
      });
    } else {
      Customer.findOne({ email })
        .then((result) => res.send(true))
        .catch((error) => res.send(error));
    }
  });
};

// NOTE: Login
let login = (req, res) => {
  const { email, password } = req.body;

  Customer.countDocuments({ email }, (err, count) => {
    if (count === 0) {
      Seller.countDocuments({ email }, (err, document) => {
        if (document === 0) {
          res.send(false);
        } else {
          Seller.findOne({ email })
            .then((result) => {
              const checkPass = bcrypt.compareSync(password, result.password);

              checkPass
                ? res.send({ accessToken: auth.generateToken(result) })
                : res.send(true);
            })
            .catch((err) => res.send(err));
        }
      });
    } else {
      Customer.findOne({ email })
        .then((result) => {
          const checkPass = bcrypt.compareSync(password, result.password);

          checkPass
            ? res.send({ accessToken: auth.generateToken(result) })
            : res.send(false);
        })
        .catch((err) => res.send(err));
    }
  });
};

// NOTE: Get single user
let getSingleUser = (req, res) => {
  const { id } = req.user;
  Customer.find({ _id: id }).then((result) => res.send(result));
};

// NOTE: Deactivate user
let deactivateUser = (req, res) => {
  const { id } = req.user;
  Customer.findByIdAndUpdate({ _id: id }, { isActive: false }, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

// NOTE: Activate user
let activateUser = (req, res) => {
  const { id } = req.user;
  Customer.findByIdAndUpdate({ _id: id }, { isActive: true }, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

// NOTE: Order item
let orderProduct = async (req, res) => {
  //I need to find out who's ordering -- i can get this from the tokebn
  //And the product they want to order -- get the id of the product
  //Update the product id and push it in customers order array
  const productObj = {
    productName: '',
    productPrice: 0,
  };

  let product = await Product.findOne({ _id: req.params.id })
    .then((result) => {
      const customer = {
        customerId: req.user.id,
      };

      productObj.productName = result.name;
      productObj.productPrice = result.price;

      result.customers.push(customer);

      return result
        .save()
        .then(() => true)
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));

  if (!product) {
    return res.send({ message: product });
  }

  let customer = await Customer.findOne({ _id: req.user.id })
    .then((result) => {
      let order = {
        productId: result.id,
        productPrice: productObj,
      };

      result.orders.push(order);

      return result
        .save()
        .then(() => true)
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));

  if (!customer) {
    return res.send({ message: customer });
  }

  let orderSum = await Customer.findOne({ _id: req.user.id })
    .then((result) => {
      let sum = [];
      result.orders.forEach((product) => {
        sum.push(product.productPrice.productPrice);
      });

      let overallSum = sum.reduce((prev, current) => prev + current);
      result.orderSum = overallSum;

      return result
        .save()
        .then(() => true)
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));

  if (product && customer && orderSum) {
    return res.send({
      message: 'Successfully placed an order',
    });
  }
};

module.exports = {
  registerCustomer,
  checkIfEmailExist,
  login,
  activateUser,
  deactivateUser,
  getSingleUser,
  orderProduct,
};
