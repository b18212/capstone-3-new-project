const Product = require('../models/Product');
const Customer = require('../models/Customer');

//view all products (active only)
const viewAllProducts = (req, res) => {
  Product.find({ isActive: true })
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};

//view single product
const viewProduct = (req, res) => {
  Product.findById(req.params.id)
    .then((data) => res.send(data))
    .catch((err) => res.send(err));
};

//find product by name
const findProduct = (req, res) => {
  Product.find({
    name: {
      $regex: req.body.name,
      $options: '$i',
    },
    isActive: true,
  }).then((data) => {
    if (data.length === 0) {
      return res.send('No item found! Please try again.');
    } else {
      return res.send(data);
    }
  });
};

//leave review
/* 
  Find the customers ID via the token and then search the productId if the customers' ID already
  exists
*/
const leaveReview = async (req, res) => {
  let productObj = {
    productName: '',
    productReview: '',
  };

  let product = await Product.findOne({ _id: req.params.id })
    .then((result) => {
      let customer = {
        customerId: req.user.id,
        message: req.body.message,
      };

      productObj.productName = result.name;
      productObj.productReview = customer.message;

      result.review.push(customer);

      return result
        .save()
        .then(() => true)
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));

  if (!product) {
    return res.send({ message: product });
  }

  let customer = await Customer.findOne({ _id: req.user.id })
    .then((result) => {
      let order = {
        productId: result.id,
        message: productObj.productReview,
      };

      result.reviews.push(order);

      return result
        .save()
        .then(() => true)
        .catch((err) => res.send(err));
    })
    .catch((err) => res.send(err));

  if (!customer) {
    return res.send({ message: customer });
  }

  if (product && customer) {
    return res.send({
      message: 'Successfully placed a review',
    });
  }
};

//view all categs (active only)
const categView = (req, res) => {
  Product.find({
    categories: {
      $regex: req.params.categories,
      $options: '$i',
    },
    isActive: true,
  }).then((data) => {
    if (data.length === 0) {
      return res.send('No item found! Please try again.');
    } else {
      return res.send(data);
    }
  });
};

const getCategories = (req, res) => {
  res.send(Product.schema.path('categories').enumValues);
};

module.exports = {
  viewAllProducts,
  viewProduct,
  findProduct,
  leaveReview,
  categView,
  getCategories,
};
