const express = require('express');
const router = express.Router();
const auth = require('../security/auth');

const { verifyToken } = auth;

const sellerControllers = require('../controllers/sellerControllers');

router.post('/add', verifyToken, sellerControllers.addProducts);

router.get('/viewAllProducts', verifyToken, sellerControllers.deleteProduct);

router.put('/edit/:id', verifyToken, sellerControllers.editProducts);

router.put('/delete/:id', verifyToken, sellerControllers.deleteProduct);

//AISHA
router.post('/register', sellerControllers.sellerRegister);

router.get('/getSingleSeller/:id', sellerControllers.viewSeller);

module.exports = router;
