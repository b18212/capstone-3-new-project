const express = require('express');
const router = express.Router();
const auth = require('../security/auth');
const { verifyToken } = auth;

const productControllers = require('../controllers/productControllers');

router.get('/viewAll', productControllers.viewAllProducts);

router.get('/search', productControllers.findProduct);

router.get('/getCategories', productControllers.getCategories);

router.get('/:id', productControllers.viewProduct);

router.post('/leaveReview/:id', verifyToken, productControllers.leaveReview);

router.get('/search/:categories', productControllers.categView);

module.exports = router;
