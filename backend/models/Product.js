const mongoose = require('mongoose');
//https://mongoosejs.com/docs/validation.html

const productSchema = new mongoose.Schema({
  sellerId: {
    type: String,
  },
  name: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  description: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  price: {
    type: Number,
    required: [true, 'Product name cannot be empty'],
  },
  quantity: {
    type: Number,
    required: [true, 'Quantity cannot be empty'],
  },
  categories: {
    type: String,
    enum: {
      values: [
        'Garden & Outdoors',
        'Toys, Children & Baby',
        'Clothes, Shoes, Jewellery & Accessories',
        'Sports & Outdoors',
        'Food & Grocery',
        'Home & Kitchen',
        'Software',
        'Computer & Accessories',
      ],
      message: '{VALUE} is not supported',
    },
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  customers: [
    {
      customerId: String,
    },
  ],
  review: [
    {
      customerId: String,
      message: String,
    },
  ],
  dateAdded: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model('Product', productSchema);
