1. User to be able to add products in their basket
2. User should be able to remove added products in the basket
3. User should be able to sign up
4. User should be login
5. User should be able to delete/deactive their accounts
6. User should be able to see the products added to their basket
7. User should be able to preview ordered products / order history
------------------------------------------------------

first name - String
last name - String
email - String
password - string
mobileNumber - string
isActive - boolean (default true)
orders - [{
    dateOfPurcahse --- find out how to format date before inserting in the database
    productId
    productPrice (initial purchase)
    totalAmount
}]
review[{
    productId

}]



-----------------------------------------------
1. Products should have a name, description, 
quantity, review , price and seller's brand name

products - [{
    sellerId
    name - string
    description - string
    price - number
    quantity - number
    customers: - array of customers who bought the product
    categories: [{

    }]
    review[{
        userId
            message
}]

-----------------------------------------------
1. Seller should be able to add products
2. Seller should be able to remove products
3. Seller should be able to set the product availability
4. Seller should be able to modify product quantity
5. Seller should be able to sign up and be granted admin privilege
6. Seller should have a different UI
7. Seller should be able to see how many products have they sold

name - string
description - string
isAdmin - boolean (default true)
products - [{
    productId - Array of products
    review[{
        userId
    }]
}]


-------------------------------------
1. Review should have the userId and productId

