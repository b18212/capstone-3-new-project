const mongoose = require("mongoose");

let customerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name and last name is required"],
  },
  lastName: {
    type: String,
    required: [true, "First name and last name is required"],
  },
  email: {
    type: String,
    required: [true, "Email cannot be empty"],
  },
  password: {
    type: String,
    min: [6, "Minimum 6 characters"],
    required: [true, "Password cannot be empty"],
  },
  mobileNumber: {
    type: String,
    required: [true, "Mobile number cannot be empty"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  orders: [
    {
      dateOfPurchase: {
        type: Date,
        default: new Date(),
      },
      productId: {
        type: String,
        required: [true, "Product Id cannot be empty"],
      },
      productPrice: {
        productName: String,
        productPrice: Number,
      },
      quantity: Number,
    },
  ],
  reviews: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      dateReviewed: {
        type: Date,
        default: new Date(),
      },
      message: {
        type: String,
        required: [true, "Review message is required"],
      },
    },
  ],
  orderSum: Number,
});

module.exports = mongoose.model("Customer", customerSchema);
//LOUIE
